import pygame
offset = 36

red = 255, 0, 0, 255
green = 0, 255, 0, 255
blue = 0, 0, 255, 255
white = 255, 255, 255, 255

screen_width = 640 + offset/2
screen_height = 576
screen_size = screen_width, screen_height
screen_centre = screen_width/2, screen_height/2
screen = pygame.display.set_mode(screen_size)

LEFT_MOUSEBUTTON = 1
RIGHT_MOUSEBUTTON = 3