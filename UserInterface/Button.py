from Element import Element
import pygame

class Button(Element):

    def __init__(self, screen, x, y, width, height, colour, message=None):
        print "class Menu item"
        self.screen = screen
        self.rect = pygame.Rect(x, y, width, height)
        self.colour = colour
        self.original_colour = colour
        self.message = message
        self.font = pygame.font.Font(None, 36)
        self.label = self.font.render(self.message, 1, (0, 255, 0))
        self.labelrect = self.label.get_rect(center=(self.centre_text_within_button()))
        self.action = None

    def draw(self):
        pygame.draw.rect(self.screen, self.colour, self.rect)
        self.label = self.font.render(self.message, 1, (0, 255, 0))
        self.screen.blit(self.label, self.labelrect)

