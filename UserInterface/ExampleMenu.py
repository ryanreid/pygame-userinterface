import Constant
from UserInterface import *

class ExampleMenu(Menu):

    def __init__(self):
        self.splash_label = Label(Constant.screen, Constant.screen_width / 3, 50, Constant.green, "Welcome to User Interface!")

        self.start_button = Button(Constant.screen, Constant.screen_width / 2 - 50, Constant.screen_height / 2 - 50, 100, 100,
                             Constant.blue, "Start")
        self.options_button = Button(Constant.screen, Constant.screen_width / 2 - 50, Constant.screen_height / 2 + 50, 100,
                              100, Constant.blue, "Options")
        self.quit_button = Button(Constant.screen, Constant.screen_width / 2 - 50, Constant.screen_height / 2 + 150,
                                     100,100, Constant.blue, "Quit")
        self.append(self.splash_label)
        self.append(self.start_button)
        self.append(self.options_button)
        self.append(self.quit_button)





