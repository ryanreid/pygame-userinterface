import abc
import pygame
import Constant

class Element(object):
    _metaclass_ = abc.ABCMeta
    overlay_colour = Constant.red

    @abc.abstractmethod
    def draw(self):
        """We need to draw"""
        return

    def get_rect(self):
        return self.rect

    def get_colour(self):
        return self.colour

    def get_original_colour(self):
        return self.original_colour

    def get_action(self):
        return self.action

    def get_overlay_colour(self):
        return self.overlay_colour

    def get_message(self):
        return self.message

    def set_message(self, new_message):
        self.message = new_message

    def set_colour(self, colour):
        self.colour = colour

    def set_action(self, action):
        self.action = action

    def centre_text_within_button(self):
        x = self.rect.centerx
        y = self.rect.centery
        return x, y

    def is_mouse_over_element(self, mouse_position):
        x = mouse_position[0]
        y = mouse_position[1]
        if (x > self.rect.left
            and x < self.rect.right
            and y > self.rect.top
            and y < self.rect.bottom):
            print "Mouse is over element: " + self.message
            return True

    def change_colour_of_element(self, mouse_position):
        if (self.is_mouse_over_element(mouse_position)):
            self.set_colour(self.get_overlay_colour())
        else:
            self.set_colour(self.get_original_colour())

    def action_when_clicked_on(self, mouse_position):
        if self.is_mouse_over_element(mouse_position):
            try:
                action = self.get_action()
                action()
            except AttributeError:
                print "I don't have an action attribute"
            except TypeError:
                print "My action hasn't been applied"

    def element_string_when_clicked_on(self, mouse_position):
        if self.is_mouse_over_element(mouse_position):
            string = self.get_message()
            return string

    def apply_action_to_element(self, message, action):
        if (self.get_message() == message):
            self.set_action(action)
        else:
            print "Action cannot be applied."