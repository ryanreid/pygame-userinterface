from Element import Element
import pygame

class Label(Element):

    def __init__(self, screen, x, y, colour, message):
        self.screen = screen
        self.colour = colour
        self.original_colour = colour
        self.message = message
        self.font = pygame.font.Font(None, 36)
        self.width, self.height = self.font.size(self.message)
        self.label = self.font.render(self.message, 1, (0, 255, 0))
        self.rect = pygame.Rect(x, y, self.width, self.height)

    def draw(self):
        self.label = self.font.render(self.message, 1, self.colour)
        self.screen.blit(self.label, self.rect)

