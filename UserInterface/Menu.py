import abc

class Menu(object):
    __metaclass_ = abc.ABCMeta
    ui_list = list()

    def append(self, ui_element):
        self.ui_list.append(ui_element)

    def draw(self):
        for i in self.ui_list:
            i.draw()

    def is_mouse_over_element(self, mouse_position):
        for i in self.ui_list:
            i.mouse_over_element(mouse_position)

    def change_colour(self, mouse_position):
        for i in self.ui_list:
            i.change_colour_of_element(mouse_position)

    def has_been_clicked_on(self, mouse_position):
        for i in self.ui_list:
            i.action_when_clicked_on(mouse_position)

    def element_string_when_clicked_on(self, mouse_position):
        for i in self.ui_list:
            return i.element_string_when_clicked_on(mouse_position)

    def apply_action(self, message, action):
        for i in self.ui_list:
            i.apply_action_to_element(message, action)